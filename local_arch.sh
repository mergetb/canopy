#!/bin/bash

declare -A M
M[x86_64]=amd64
M[amd64]=amd64
M[armv7l]=arm5

echo ${M[`uname -m`]}
