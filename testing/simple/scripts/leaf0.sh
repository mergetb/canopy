#!/bin/bash
# LEAF0

# interfaces
net add loopback lo ip address 172.0.2.1/24
net add interface swp3 ip address 10.0.0.2/30

# routing
net add bgp autonomous-system 64501
net add bgp router-id 10.0.0.2
net add bgp neighbor 10.0.0.1 remote-as 64500
net add bgp evpn neighbor 10.0.0.1 activate
net add bgp redistribute connected
net add bgp evpn advertise-all-vni

# downlinks
net add bridge bridge ports swp1
net add bridge bridge ports swp2

net commit
