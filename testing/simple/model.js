
function cumulus(name) {
  return {
    'name': name,
    'image': 'cumulusvx-3.5-mvrf',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) },
    'mounts': [{ 'source': env.CANOPY, 'point': '/tmp/canopy' }]
  };
}

function fedora(name) {
  return {
    'name': name,
    'image': 'fedora-28',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(2) },
    'mounts': [{ 'source': env.CANOPY, 'point': '/tmp/canopy' }]
  };
}

topo = {
  name: 'canopy-simple',
  nodes: [fedora('a'), fedora('b'), fedora('c'), fedora('d')],
  switches: [cumulus('leaf0'), cumulus('leaf1'), cumulus('spine')],
  links: [
    Link('a', 1, 'leaf0', 1),
    Link('b', 1, 'leaf0', 2),
    Link('c', 1, 'leaf1', 1),
    Link('d', 1, 'leaf1', 2),
    Link('leaf0', 3, 'spine', 1),
    Link('leaf1', 3, 'spine', 2)
  ]
}

