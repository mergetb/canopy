#!/bin/sh

cd /tmp/canopy

for switch in ca0 ca3; do
  for port in swp1 swp2 swp3 swp4 swp5; do
    ./canopy set port $switch $port up
  done
done

for switch in cf0 cf1; do
  for port in swp1 swp2 swp3 swp4; do
    ./canopy set port $switch $port up
  done
done

for switch in csp0; do
  for port in swp1 swp2 swp3; do
    ./canopy set port $switch $port up
  done
done
