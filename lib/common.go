package canopy

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"
)

func Any(*Element) bool {
	return true
}

func List(filter func(e *Element) bool) ([]*Element, error) {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Error("failed to open default context")
		return nil, err
	}
	defer ctx.Close()

	links, err := rtnl.ReadLinks(ctx, nil)
	if err != nil {
		log.WithError(err).Error("failed to read links")
		return nil, err
	}

	bridgeMap, err := ifmap(ctx, unix.AF_BRIDGE)
	if err != nil {
		return nil, err
	}

	var result []*Element
	for _, link := range links {

		var vlanAccess int32 = 0
		brinfo, ok := bridgeMap[link.Msg.Index]
		if ok {
			if brinfo.Info.Pvid != 0 {
				vlanAccess = int32(brinfo.Info.Pvid)
			}
		}

		var tagged []int32
		if ok {
			for _, t := range brinfo.Info.Tagged {
				tagged = append(tagged, int32(t))
			}
		}

		var untagged []int32
		if ok {
			for _, t := range brinfo.Info.Untagged {
				untagged = append(untagged, int32(t))
			}
		}

		state := UpDown_Down
		if (link.Msg.Flags&unix.IFF_UP) != 0 && (link.Msg.Flags&unix.IFF_LOWER_UP) != 0 {
			state = UpDown_Up
		}

		master := ""
		if link.Info.Master != 0 {
			m, err := rtnl.GetLinkByIndex(ctx, int32(link.Info.Master))
			if err != nil {
				log.Errorf("get-link %v", err.Error)
			} else {
				master = m.Info.Name
			}
		}

		if link.Info.Type() == rtnl.PhysicalType {

			e := &Element{
				Name: link.Info.Name,
				Value: &Element_Port{
					&Port{
						State:    state,
						Access:   vlanAccess,
						Tagged:   tagged,
						Untagged: untagged,
						Mtu:      int32(link.Info.Mtu),
						Bridge:   master,
					},
				},
			}

			if filter(e) {
				result = append(result, e)
			}

		}

		if link.Info.Type() == rtnl.VxlanType {

			dev := ""
			if link.Info.Vxlan.Link != 0 {
				l, err := rtnl.GetLinkByIndex(ctx, int32(link.Info.Vxlan.Link))
				if err != nil {
					log.Errorf("get-link %v", err.Error)
				} else {
					dev = l.Info.Name
				}
			}

			learning := OnOff_Off
			if link.Info.Vxlan.Learning > 0 {
				learning = OnOff_On
			}

			result = append(result, &Element{
				Name: link.Info.Name,
				Value: &Element_Vtep{
					&Vtep{
						State:        state,
						Vni:          int32(link.Info.Vxlan.Vni),
						Bridge:       master,
						Device:       dev,
						LocalIP:      link.Info.Vxlan.Local.String(),
						Learning:     learning,
						BridgeAccess: vlanAccess,
						Mtu:          int32(link.Info.Mtu),
						Access:       vlanAccess,
						Tagged:       tagged,
						Untagged:     untagged,
					},
				},
			})

		}

		if link.Info.Type() == rtnl.BridgeType {
			result = append(result, &Element{
				Name: link.Info.Name,
				Value: &Element_Bridge{
					&Bridge{
						State: state,
					},
				},
			})
		}

	}

	return result, nil

}

func ifmap(ctx *rtnl.Context, family uint8) (map[int32]*rtnl.Link, error) {

	spec := rtnl.NewLink()
	spec.Msg.Family = family
	ifInfo, err := rtnl.ReadLinks(ctx, spec)
	if err != nil {
		log.WithError(err).Error("failed to read interface info")
		return nil, err
	}

	ifMap := make(map[int32]*rtnl.Link)
	for _, bi := range ifInfo {
		ifMap[bi.Msg.Index] = bi
	}

	return ifMap, nil

}
