package canopy

import (
	"fmt"
	"io/ioutil"
	"os/user"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Switches []string
}

func ReadConfig() (*Config, error) {

	u, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("error getting user: %v", err)
	}

	if u == nil {
		return nil, fmt.Errorf("user is nil?")
	}

	src, err := ioutil.ReadFile(
		fmt.Sprintf("%s/.config/canopy.yml", u.HomeDir))
	if err != nil {
		return nil, fmt.Errorf("error reading config: %v", err)
	}

	c := &Config{}
	err = yaml.Unmarshal(src, c)
	if err != nil {
		return nil, fmt.Errorf("error parsing config: %v", err)
	}

	return c, nil

}
