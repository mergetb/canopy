#!/bin/bash

set -e

docker build $BUILD_ARGS -f debian/builder.dock -t canopy-builder .
docker run -v `pwd`:/canopy canopy-builder /canopy/build-deb.sh
