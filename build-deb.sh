#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/canopy*.build*
rm -f build/canopy*.change
rm -f build/canopy*.deb

debuild -e V=1 -e prefix=/usr -e arch=amd64 $DEBUILD_ARGS -aamd64 -i -us -uc -b
debuild -e V=1 -e prefix=/usr -e arch=arm7 $DEBUILD_ARGS -aarmhf -i -us -uc -b
debuild -e V=1 -e prefix=/usr -e arch=arm5 $DEBUILD_ARGS -aarmel -i -us -uc -b

mv ../canopy*.build* build/
mv ../canopy*.changes build/
mv ../canopy*.deb build/
